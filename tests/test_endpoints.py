import pytest
import requests
from bs4 import BeautifulSoup

class TestApi:

    @pytest.fixture(scope='session')
    def session_api(self):
        session_api = requests.Session()
        return session_api

    @pytest.fixture(scope='class')
    def url(self):
        return 'http://127.0.0.1:5000/'

    @pytest.fixture(scope='class')
    def csrf_token(self, url, session_api):
        response = session_api.get(url + f'login/')
        soup = BeautifulSoup(response.content, 'html.parser')
        tag = soup.find(id="csrf_token")
        return (tag.get('value'))

    def test_post_signup(self, url, csrf_token, session_api):
        url_signup= url + f'signup/'
        payload=f'email=murilocaldeira@hotmail.com&username=murilocaldeira&password=1212&confirm_password=1212& TAG&csrf_token={csrf_token}&submit=Signup'
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
           }
        response = session_api.post(url_signup, headers=headers, data=payload)
        assert response.url == 'http://127.0.0.1:5000/profile/'
        assert response.ok
        
    def test_post_login(self, url, csrf_token, session_api):
        url_login = url+f'login/'
        payload=f'username=murilocaldeira&password=1212&csrf_token={csrf_token}&submit=True'
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
           }
        response = session_api.post(url_login, headers=headers, data=payload)
        assert response.url == 'http://127.0.0.1:5000/profile/'
        assert response.ok

    def test_post_newtag(self, url, csrf_token, session_api):
        url_newtag= url + f'tags/add/'
        payload=f'tag=PYTEST TAG&csrf_token={csrf_token}&submit=Add Tag'
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
           }
        response = session_api.post(url_newtag, headers=headers, data=payload)
        assert response.url == 'http://127.0.0.1:5000/profile/'
        assert response.ok

    def test_post_newnote(self, url, csrf_token, session_api):
        url_newnote = url + f'notes/add/'
        payload=f'note_title=PYTEST NOTE TITLE&tags=1&note=TESTS PYTEST NOTE&csrf_token={csrf_token}&submit=Add Note'
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
           }
        response = session_api.post(url_newnote, headers=headers, data=payload)
        assert response.url == 'http://127.0.0.1:5000/profile/'
        assert response.ok

    def test_post_change_email(self, url, csrf_token, session_api):
        url_change_email = url + f'profile/settings/change_email/'
        payload=f'csrf_token={csrf_token}&email=murilocaldeira@gmail.com&submit=Update Email'
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
           }
        response = session_api.post(url_change_email, headers=headers, data=payload)
        assert response.url == 'http://127.0.0.1:5000/profile/settings/'
        assert response.ok

    def test_post_change_pass(self, url, csrf_token, session_api):
        url_change_pass = url + f'profile/settings/change_password/'
        payload=f'csrf_token={csrf_token}&password=123&confirm_password=123&submit=Update Password'
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
           }
        response = session_api.post(url_change_pass, headers=headers, data=payload)
        assert response.url == 'http://127.0.0.1:5000/profile/settings/'
        assert response.ok

    def test_post_edit_note(self, url, csrf_token, session_api):
        url_edit_note = url + f'notes/edit/1/'
        payload=f'note_title=PYTEST NOTE TITLE EDITED&tags=1&note=TESTS PYTEST NOTE EDITED&csrf_token={csrf_token}&note_id=1'
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
           }
        response = session_api.post(url_edit_note, headers=headers, data=payload)
        assert response.url == 'http://127.0.0.1:5000/profile/'
        assert response.ok

    def test_get_del_tag(self, url, csrf_token, session_api):
        url_del_tag = url + f'tags/delete/1/'
        payload=f'csrf_token={csrf_token}'
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
           }
        response = session_api.get(url_del_tag, headers=headers, data=payload)
        assert response.url == 'http://127.0.0.1:5000/tags/delete/1/'
        assert response.ok

    def test_get_del_note(self, url, csrf_token, session_api):
        url_del_note = url + f'notes/delete/1/'
        payload=f'csrf_token={csrf_token}'
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
           }
        response = session_api.get(url_del_note, headers=headers, data=payload)
        assert response.url == 'http://127.0.0.1:5000/notes/delete/1/'
        assert response.ok

    def test_get_logout(self, url, csrf_token, session_api):
        url_logout = url + f'logout/'
        payload=f'csrf_token={csrf_token}'
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
           }
        response = session_api.get(url_logout, headers=headers, data=payload)
        assert response.url == 'http://127.0.0.1:5000/logout/'
        assert response.ok