# A-Simple-Note-Taking-Web-App
An easy to use and deploy web app built using Flask

# Initializing locally
Creating a VENV:

`conda create -n note-taking`

`conda activate note-taking`

`conda install python=3.7`

`Installing the dependencies:`

`pip install -r requirements.txt`

Starting the app:

`python manage.py`

Visit `0.0.0.0:5000` on your web browser

# Changes and Fixes: 

## Code changes:

Changed Flask port from "5000" to 5000 (string to int)

Excluded variable CUSTOM_DATE because it was not properly defined

## Updating libraries :

`pip install upgrade-requirements`

`upgrade-requirements`

`pip install email_validator` (because the newer WTForms==2.3.3)

`pip freeze > requirements.txt`


## Tests

Created ./tests/test_endpoints.py


Installing pytest and requests lib's:

  `pip install pytest`

  `pip install requests`

  `pip install beautifulsoup4`

  `pip freeze > requirements.txt`

  
Running the tests: 

  `pytest -v tests/`


# Docker

Created the Dockerfile

Building the Docker Image:

`docker build -t note-taking-bric-abode:0.0.1 .`

Running the container:

`docker run -p 5000:5000 -ti note-taking-bric-abode:0.0.1`

# Pipeline

Created the .gitlab-ci.yml

Throwing the pipeline: 

`git status`
`git add . `
`git commit -m "my last changes"`
`git push -u origin master`

## Pylint

Installing Pylint runner:

`pip install pylint_runner`

Runing the lint:

`pylint_runner -v`

## Safety Check (CVE Scan)

Installing Safety:

`pip install safety`

Runing the CVE Scan:

`safety_check`

## Tests

Running application in background:

`nohup python -u manage.py &`

Running the tests:

`pytest -v tests/`


## Build and Publish on Docker Hub


On the GitLab project, go to Settings => CI/CD, click on Expand in the Variables section and add the following variables: 

CI_REGISTRY_USER must contain your Docker HUB username.

CI_REGISTRY_PASSWORD must contain your Docker HUB password or your access token.

CI_REGISTRY must contain "docker.io" .

CI_REGISTRY_IMAGE must contain "index.docker.io/mcaldeiragoes/note-taking-brick-abode" .


Commands on .gitlab-ci.yml:

` - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY`

` - docker build --pull -t "$CI_REGISTRY_IMAGE"`.

` - docker push "$CI_REGISTRY_IMAGE"`

## Deploy


On the GitLab project, go to Settings => CI/CD, click on Expand in the Variables section and add the following variables: 

AWS_ACCESS_KEY must contain the AWS access key ID

AWS_SECRET_KEY must contain the AWS secret key

AWS_EB_ENVIRONMENT must contain the Elastic Beanstalk environment name


Created Dockerrun.aws.json file to deploy the Docker Image.

Created .elasticbeanstalk folder for the Elastic Beanstalk environment settings. 

Commands on .gitlab-ci.yml:

` - aws configure set aws_access_key_id $AWS_ACCESS_KEY`

`- aws configure set aws_secret_access_key $AWS_SECRET_KEY`

` - aws configure set region us-east-1`

` eb deploy "$AWS_EB_ENVIRONMENT" `


For the first time you are deploying on AWS, you must to run this command before, to create the Elastic Beanstalk environment: 

`eb create -c "note-taking-$AWS_EB_ENVIRONMENT" "$AWS_EB_ENVIRONMENT"`

# Cloud Provider, Reverse Proxy, Domain and DNS 

Using AWS as a cloud provider and the Elastic Beanstalk service.

Elastic Beanstalk uses nginx as the reverse proxy to map your application to your load balancer on port 80. 

Elastic Beanstalk provides a default nginx configuration that you can either extend or override completely with your own configuration.

By default, Elastic Beanstalk configures the nginx proxy to forward requests to your application on port 5000. 

The default nginx proxy settings are OK to this project. 

By default, your environment is available to users at a subdomain of elasticbeanstalk.com. When you create an environment, you can choose a hostname for your application. The subdomain and domain are auto populated to region.elasticbeanstalk.com.

To route users to your environment, Elastic Beanstalk registers a CNAME record that points to your environment's load balancer:

[note-taking-eb-notetakingeb-env.us-east-1.elasticbeanstalk.com](http://note-taking-eb-notetakingeb-env.us-east-1.elasticbeanstalk.com/)

I also created a NO-IP free hostname to redirect the web requests to the Elastic Beanstalk instance:

[notetakingba.ddns.net](http://notetakingba.ddns.net)
