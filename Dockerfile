FROM ubuntu:18.04

RUN apt-get update -y && \
    apt-get install -y python-pip python-dev

COPY ./requirements.txt /note-taking-brick-abode/requirements.txt

WORKDIR /note-taking-brick-abode

RUN pip install -r requirements.txt

COPY . /note-taking-brick-abode

ENTRYPOINT [ "python" ]

CMD [ "manage.py" ]